<?php
 session_start();
 
if(!isset($_SESSION['autenticado']))
  header("location:login.php");
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<!DOCTYPE html>
<html ng-app="app">
	<head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
            <meta name="author" content="Coderthemes">

            <link rel="shortcut icon" href="assets/images/favicon_1.ico">

            <title>CLab - Economize Tempo e Dinheiro com as suas compras</title>

            <link href="app/bower_components/ng-dialog/css/ngDialog.css" rel="stylesheet" type="text/css" />
            <link href="app/bower_components/ng-dialog/css/ngDialog-theme-default.css" rel="stylesheet" type="text/css" />
            <link href="app/bower_components/angular-responsive-tables/release/angular-responsive-tables.css" rel="stylesheet" type="text/css" />
            <link href="app/bower_components/angular-ui-select/dist/select.css" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" href="https://npmcdn.com/angular-toastr/dist/angular-toastr.css" />
            
            <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
            <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
            <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
            <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
            <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
            <link href="app/custom/angucomplete.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

	</head>

	<body class="fixed-left">

		<!-- Begin page -->
		<div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="index.html" class="logo"><i class="icon-magnet icon-c-logo"></i><span>CLab</span></a>
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left">
                                    <i class="ion-navicon"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>
                            
                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="dropdown hidden-xs">
                                    <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                        <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-lg">
                                        <li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notificação</li>
                                        <li class="list-group nicescroll notification-list">
                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-diamond fa-2x text-primary"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                                    <p class="m-0">
                                                        <small>There are new settings available</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-cog fa-2x text-custom"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">New settings</h5>
                                                    <p class="m-0">
                                                        <small>There are new settings available</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-bell-o fa-2x text-danger"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">Updates</h5>
                                                    <p class="m-0">
                                                        <small>There are <span class="text-primary font-600">2</span> new updates available</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-user-plus fa-2x text-info"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">New user registered</h5>
                                                    <p class="m-0">
                                                        <small>You have 10 unread messages</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-diamond fa-2x text-primary"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                                    <p class="m-0">
                                                        <small>There are new settings available</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                            <a href="javascript:void(0);" class="list-group-item">
                                                <div class="media">
                                                    <div class="pull-left p-r-10">
                                                     <em class="fa fa-cog fa-2x text-custom"></em>
                                                    </div>
                                                    <div class="media-body">
                                                      <h5 class="media-heading">New settings</h5>
                                                      <p class="m-0">
                                                        <small>There are new settings available</small>
                                                    </p>
                                                    </div>
                                              </div>
                                           </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="list-group-item text-right">
                                                <small class="font-600">Veja todas as mensagens</small>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="hidden-xs">
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                </li>
                               
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                                     <i class="icon-settings"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Meus Dados</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-key m-r-5"></i> Alterar Senha</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Sair</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                            <li class="text-muted menu-title">Navegação</li>

                            <li disable>
                                <a href="#/" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> </a>                             
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="icon-rocket"></i> <span> Cadastros </span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="#lista-mercados">Mercados</a></li>                                    
                                    <li><a href="#lista-produtos">Produtos</a></li>
                                    <li><a href="#marca"><span>Marcas</span></a></li>
                                    <li><a href="#composicao"><span>Composição</span></a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="#lista-compra" ><i class="ti-shopping-cart"></i><span class="label label-primary pull-right">0</span><span> Listas de Compra </span> </a>                           
                            </li>

                            <li>
                                <a href="#cotacao-manager" class="waves-effect"><i class="ti-pencil-alt"></i> <span> Cotações </span> </a>
                            </li>

                            <li>
                                <a href="#meus-alertas" class="waves-effect"><i class="icon-bell"></i><span class="label label-danger pull-right">10</span> <span> Meus Alertas </span> </a>
                            </li>
                            
                        

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-user"></i><span> Meu Perfil </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="#"> Editar Dados</a></li>
                                    <li><a href="#"> Alterar Senha</a></li>
                                </ul>
                            </li>

                         

                            <li class="text-muted menu-title">Suporte</li>

                            <li>
                                <a href="#" class="waves-effect"><i class="icon-phone"></i><span> Abrir Chamado </span></a>                                
                            </li>
                            <li>
                                <a href="#" class="waves-effect"><i class="icon-call-end"></i><span> Meus Chamados </span></a>                                
                            </li>
                            

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
			<!-- Left Sidebar End -->

			<!-- ============================================================== -->
			<!-- Start right Content here -->
			<!-- ============================================================== -->
			<div class="content-page">
				<!-- Start content -->
				<div class="content">
					<div class="container">                                            
                                            <div ng-view></div>
                                        </div>
                               
                                </div>
                <footer class="footer">
                    <center> <?php echo '2016'   ; ?> © CLab - Desenvolvido por <a href="http://www.vox3.com.br" target="_blank">VOX3 Tecnologia</a></center>
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


          


        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>


        <script src="app/bower_components/string-mask/src/string-mask.js"></script>
        <!-- Angular -->
        <script src="app/bower_components/angular/angular.js"></script>
        <script src="app/bower_components/angular-locale-pt-br/angular-locale_pt-br.js"></script>
        <script src="https://code.angularjs.org/1.5.0-rc.1/angular-route.js"></script>
        <script src="app/bower_components/ng-dialog/js/ngDialog.js"></script>
        <script src="https://npmcdn.com/angular-toastr/dist/angular-toastr.tpls.js"></script>
        <script src="app/bower_components/angular-base64-upload/dist/angular-base64-upload.js"></script>
        <script src="app/bower_components/angular-responsive-tables/release/angular-responsive-tables.min.js"></script>
        <script src="app/bower_components/angular/angular.js"></script>
        <script src="app/bower_components/angular-ui-select/dist/select.min.js"></script>
        <script src="app/bower_components/angular-input-masks/angular-input-masks-standalone.js"></script>
        
        <script src="app/app.js"></script>
        <script src="app/util/uploadService.js"></script>
        <script src="app/custom/angucomplete.js"></script>
        <script src="app/custom/busca-produto-diretiva/buscaProdutoApiService.js"></script>
        <script src="app/custom/busca-produto-diretiva/buscaProdutoDiretiva.js"></script>
        
        <script src="app/views/dashboard/controller/dashboardController.js"></script>
        <script src="app/views/composicao/controller/composicaoController.js"></script>
        <script src="app/views/composicao/service/composicaoService.js"></script>
        
         <script src="app/views/marca/controller/marcaController.js"></script>
         <script src="app/views/marca/service/marcaApiService.js"></script>
         
         <script src="app/views/produto/controller/listaProdutosController.js"></script>
         <script src="app/views/produto/controller/produtoController.js"></script>
         <script src="app/views/produto/service/produtoApiService.js"></script>
         
         <script src="app/views/mercado/controller/mercadoController.js"></script>
         <script src="app/views/mercado/service/mercadoApiService.js"></script>

         <script src="app/views/listacompra/controller/listaCompraController.js"></script>
         <script src="app/views/listacompra/service/listaCompraApiService.js"></script>


         <script src="app/views/cotacaomanager/controller/cotacaoManagerController.js"></script>
         <script src="app/views/cotacaomanager/service/cotacaoManagerService.js"></script>


         <script src="app/views/meusalertas/controller/meusalertasController.js"></script>
         <script src="app/views/meusalertas/service/meusalertasService.js"></script>
         
         <script src="app/views/cotacao/controller/cotacaoController.js"></script>
         <script src="app/views/cotacao/service/cotacaoService.js"></script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>


        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        
        <script type="text/javascript">
            var __path = '<?php  
            $host = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_SPECIAL_CHARS); 
            $subdominio = filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_SPECIAL_CHARS); 
            
            $subdominio = str_replace("/index.php", "", $subdominio);
            $subdominio = str_replace("#", "", $subdominio);
            
            $ultimaLetraHost = substr($host,strlen($host)-1);
            if( $ultimaLetraHost === "/"){
                $host = substr($host,0,strlen($host)-1);
            }
            $server = "http://" . $host;
            
            if($subdominio != ""){
                $server = $server . "" .$subdominio;
            }
            
            $url =  $server;
            
            echo $url;
                
                ?>';
        </script>
	
	</body>
</html>


