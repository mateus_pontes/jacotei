<?php

require_once 'vendor/autoload.php';
require_once 'dao/Daos.php';
require_once 'services/Services.php';

$app = new Slim\App();

$mw = function ($request, $response, $next) {
    /*$headers = apache_request_headers();

    if(isset($headers['Authorization'])) {
        session_start();
        if (array_key_exists($headers['Authorization'], $_SESSION)){
            _setCurrentUser($headers['Authorization']);
            $response = $next($request, $response);
        }
        else
           $response = _returnLoginError($response);
    } else
       $response = _returnLoginError($response);

    return $response;*/
    session_start(); 
    if(!isset($_SESSION['autenticado'])){
        $response->withStatus(401);
    }else{
       $response = $next($request, $response); 
    }
    
    return $response;
};

$app->get('/', function ($request, $response, $args) {
    $response->write("Já Cotei API versão 1.0");
});

$app->post('/autenticar', function ($request, $response, $args) {
    
    $data = json_decode($request->getBody());
    session_start();
    $_SESSION['autenticado'] = true;
    
    return $response->withStatus(200);;
});

$app->get('/composicao', function ($request, $response, $args) {
    $service = new ComposicaoService();
    $result = $service->getAll();
    $response->write(json_encode($result));

    return $response;
});

$app->post('/composicao/add', function ($request, $response, $args) {
    
    $data = json_decode($request->getBody());
    $service = new ComposicaoService();
    $result = $service->save($data);
    $_SESSION['autenticado'] = true;
    
    return $response->withStatus(200);;
})->add($mw);

$app->get('/marca', function ($request, $response, $args) {
    $service = new MarcaService();
    $result = $service->getAll();
    $response->write(json_encode($result));

    return $response;
})->add($mw);

$app->post('/marca/add', function ($request, $response, $args) {
    
    $data = json_decode($request->getBody());
    $service = new MarcaService();
    $result = $service->save($data);
    
    return $response->withStatus(200);;
});

$app->get('/mercado', function ($request, $response, $args) {
    $service = new MercadoDao();
    $service->__contruct();
    
    $result = $service->getAll();
    $response->write(json_encode($result));

    return $response;
})->add($mw);

$app->get('/mercado/{id}', function ($request, $response, $args) {
    $codigo = $request->getAttribute('id');
    $service = new MercadoService();
    $result = $service->getId($codigo);
    
    $response->write(json_encode($result));
})->add($mw);

$app->post('/mercado/add', function ($request, $response, $args) {    
    $data = json_decode($request->getBody());
    $service = new MercadoService();
    $result = $service->save($data);
    
    $response->write(json_encode($result));
});

$app->post('/produto/add', function ($request, $response, $args) {
    $data = json_decode($request->getBody());
    $service = new ProdutoService();
    $result = $service->save($data);
    
    $response->write(json_encode($result));
})->add($mw);

$app->get('/produto/{id}', function ($request, $response, $args) {
    $codigo = $request->getAttribute('id');
    $service = new ProdutoService();
    $result = $service->getId($codigo);
    
    $response->write(json_encode($result));
})->add($mw);

$app->post('/produto/buscar', function ($request, $response, $args) {
    $data = json_decode($request->getBody());
    $service = new ProdutoService();
    $result = $service->buscar($data);
    
    $response->write(json_encode($result));
})->add($mw);

$app->post('/produto/filtro', function ($request, $response, $args) {
    $data = json_decode($request->getBody());
    $service = new ProdutoService();
    $result = $service->getFilterProd($data->filtro);
    
    $response->write(json_encode($result));
});

$app->get('/listacompra', function ($request, $response, $args) {
    //$codigo = $request->getAttribute('id');
    $service = new ListaCompraService();
    $result = $service->getAll();
    
    $response->write(json_encode($result));
});

$app->post('/listacompra/codigo', function ($request, $response, $args) {
    $data = json_decode($request->getBody());
    $service = new ListaCompraService();
    $result = $service->getById($data->codigo);
    
    $response->write(json_encode($result));
});

$app->get('/listacompra/minhalista', function ($request, $response, $args) {
    //$codigo = $request->getAttribute('id');
    $service = new ListaCompraService();
    $result = $service->getLists();
    
    $response->write(json_encode($result));
});

$app->post('/listacompra/add', function ($request, $response, $args) {
   $data = json_decode($request->getBody());
    $service = new ListaCompraService();
    $result = $service->createList($data);
    
    $response->write(json_encode($result));
});



$app->get('/alerta', function ($request, $response, $args) {
    //$codigo = $request->getAttribute('id');
    $service = new AlertaService();
    $result = $service->getMeusAlertas();
    
    $response->write(json_encode($result));
});

$app->post('/alerta/add', function ($request, $response, $args) {
    $data = json_decode($request->getBody());
    $service = new AlertaService();
    $result = $service->save($data);
    
    $response->write(json_encode($result));
});

$app->post('/cotacao/add', function ($request, $response, $args) {
    $data = json_decode($request->getBody());
    $service = new CotacaoManagerService();
    $result = $service->addCotacao($data);
    
    $response->write(json_encode($result));
});

$app->post('/cotacao/item/add', function ($request, $response, $args) {
    $data = json_decode($request->getBody());
    $service = new CotacaoManagerService();
    $result = $service->addItemCotacao($data);
    
    $response->write(json_encode($result));
});


$app->post('/upload/image', function ($request, $response, $args) {
    $data = json_decode($request->getBody());
    $service = new UploadService();
    $result = $service->image($data);
    
    $response->write(json_encode($result));
})->add($mw);

function _returnLoginError($response){
    $errorResult = new stdClass();
    $errorResult->message = "Acesso Negado !";
    $newResponse = $response->withStatus(401);
    
    return $newResponse;
}

function _setCurrentUser($token){
    $_SESSION['currentUser'] = $_SESSION[$token];
}
$app->run();
