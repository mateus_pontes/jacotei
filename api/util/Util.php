<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Util
 *
 * @author luisfilipe
 */
class Util {
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    public static function geraTimestamp($data) {
        $partes = explode('/', $data);
        return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
    }
    
    public static function in_array_field($needle, $needle_field, $haystack) { 
        foreach ($haystack as $item) {
            $obj = json_decode(json_encode($item), FALSE);
            if (isset($obj->$needle_field) && $obj->$needle_field == $needle) {   
                return true; 
            }
        }
        return false; 
    } 
}
