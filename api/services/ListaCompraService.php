<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ListaCompraService
 *
 * @author Mateus Pontes
 */
class ListaCompraService {
    public function getAll(){
        $dao = new ListaCompraDao();
        
        try{
            $dao->__contruct();
            $result = $dao->getAll();
            
            return array (
                "error" => false,
                "result" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
    public function getLists(){
        $dao = new ListaCompraDao();
        
        try{
            $dao->__contruct();
            $result = $dao->getLists();
            
            return array (
                "error" => false,
                "result" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
    public function getById($codigo){
        $dao = new ListaCompraDao();
        
        try{
            $dao->__contruct();
            $result = $dao->getById($codigo);
            
            return array (
                "error" => false,
                "result" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
    public function createList($data){
        $dao = new ListaCompraDao();
        
        try{
            $dao->__contruct();
            $codigoLista = $dao->createList($data);
            
            $dao->deleteList($codigoLista);
            
            for($i = 0; $i < count($data->itens); $i++){
                $result = $dao->addItemList($data->itens[$i], $codigoLista);
            }   
            
            return array (
                "success" => true,
                "codigoLista" => $codigoLista
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
}
