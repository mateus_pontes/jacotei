<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProdutoService
 *
 * @author Mateus Pontes
 */
class ProdutoService {
   public function save($data){
        $dao = new ProdutoDao();
        
        try{
            $dao->__contruct();
            $result = $dao->save($data);
            
            return array (
                "error" => false,
                "codigo" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
   public function buscar($data){
        $dao = new ProdutoDao();
        
        try{
            $dao->__contruct();
            $result = $dao->buscar($data);
            
            return array (
                "error" => false,
                "result" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
    public function getFilterProd($filtro){        
        $dao = new ProdutoDao();
        
        try{
            $dao->__contruct();
            $result = $dao->getFilterProd($filtro);
            
            return array (
                "error" => false,
                "result" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
    public function getId($codigo){
        $dao = new ProdutoDao();
        
        try{
            $dao->__contruct();
            $result = $dao->getId($codigo);
            
            return array (
                "error" => false,
                "result" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
}
