<?php
/**
 * Description of UploadService
 *
 * @author Mateus Pontes
 */
class UploadService {
     public function image($param){
        
        $output_file = "content/upload/file/" . uniqid() . ".png";
         
        $ifp = fopen($output_file, "wb"); 
        $data = explode(',', $param->base64);

        fwrite($ifp, base64_decode($param->base64)); 
        fclose($ifp); 
         
        try{
            return array (
                "error" => false,
                "path" => $output_file
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
    }
}
