<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MarcaService
 *
 * @author Mateus Pontes
 */
class MercadoService {
   public function getAll(){
        $dao = new MercadoDao();
        $dao->__contruct();
        
        $result = $dao->getAll();
        $dao->__dispose();
        
        return $result;
    }
    
     public function save($data){
        $dao = new MercadoDao();
        $dao->__contruct();
        
        $locationService = new SpatialHelperService();
        $data->location = $locationService->getLocation($data->endereco . ", " . $data->bairro . ", " . $data->cidade);
        
        $result = null;
        
        try{
            $dao->__contruct();
            $result = $dao->save($data);
            
            return array (
                "error" => false,
                "codigo" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
    public function getId($codigo){
        $dao = new MercadoDao();
        
        try{
            $dao->__contruct();
            $result = $dao->getId($codigo);
            
            return array (
                "error" => false,
                "result" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
}
