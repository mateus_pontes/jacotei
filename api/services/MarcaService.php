<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MarcaService
 *
 * @author Mateus Pontes
 */
class MarcaService {
   public function getAll(){
        $dao = new MarcaDao();
        $dao->__contruct();
        
        $result = $dao->getAll();
        $dao->__dispose();
        
        return $result;
    }
    
     public function save($data){
        $dao = new MarcaDao();
        $dao->__contruct();
        
        $result = null;
        
        try{
            $result = $dao->save($data);
        }
        
        catch (Exception $e){
            
        }
        
        finally {
          $dao->__dispose();  
          
          return $result;
        }
    }
}
