<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CotacaoManagerService
 *
 * @author Mateus Pontes
 */
class CotacaoManagerService {
    public function addCotacao($data){
        $dao = new CotacaoManagerDao();
        
        try{
            $dao->__contruct();
            $result = $dao->addCotacao($data);
            
            return array (
                "error" => false,
                "codigoCotacao" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
    public function addItemCotacao($data){
        $dao = new CotacaoManagerDao();
        
        try{
            $dao->__contruct();
            $result = $dao->addItemCotacao($data);
            
            return array (
                "error" => false,
                "codigo" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
}
