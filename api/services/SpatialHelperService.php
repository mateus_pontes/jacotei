<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SpatialHelperService
 *
 * @author Mateus Pontes
 */
class SpatialHelperService {
    public function getLocation($endereco){
        $endereco = urlencode($endereco);
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $endereco . "&key=AIzaSyDr8DA6V3AOT6GTpAC0O8bC6xiSbbqvNpQ";
        
        $json = file_get_contents($url);
        
        $json = json_decode($json);
        $lat = $json->results[0]->geometry->location->lat;
        $lng = $json->results[0]->geometry->location->lng;
        
        return array (                
                "lat" => $lat,
                "lng" => $lng
            );
    }
}
