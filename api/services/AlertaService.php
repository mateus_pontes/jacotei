<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AlertaService
 *
 * @author Mateus Pontes
 */
class AlertaService {
     public function save($data){
        $dao = new AlertaDao();
        
        try{
            $dao->__contruct();
            $dao->delete($data->codigoEmpresa);
                    
            for($i = 0; $i < count($data->itens); $i++){
                $result = $dao->save($data->itens[$i]);
            }            
            
            return array (
                "error" => false
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }
    
    public function getMeusAlertas(){
        $dao = new AlertaDao();
        
        try{
            $dao->__contruct();
            $result = $dao->getMeusAlertas();
            
            return array (
                "error" => false,
                "result" => $result
            );
        }
        
        catch (Exception $e){
            return array(
                "error" => true,
                "message" => $e->getMessage()
            );
        }
        
        finally {
          $dao->__dispose();
        }
    }    
}
