<?php
    class dataBaseHelper{
        protected $con;

        public function __contruct(){

        }

        public function open(){
            include("config/config.inc.php");
            $this->con = new PDO('mysql:host=mysql.18mtsistemas.com.br;dbname=18mtsistemas06;charset=utf8', $user, $pswd);
            $this->con->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
            //mysql_select_db(, $this->con);
        }

        public function getConnection(){
            return $this->con;
        }

        public function close(){
            $this->con = null;
        }

        public function status(){
            if(!$this->con){
                return "not connected";
            }else{
                return "connected!";
            }
        }

        public function query($sql){
            $stmt = $this->con->prepare($sql);
            $stmt->execute();
            
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }

        public function queryParams($sql, $params){
            $stmt = $this->con->prepare($sql);
            $stmt->execute($params);
            
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $pos = strrpos($sql, "insert into");
            
            if($pos === false){
               return $result;   
            }else{
                return (int)$this->con->lastInsertId();
            }
        }

        public function queryParamsNoFetch($sql, $params){
            return $queryResult = mysql_query_params($this->con, $sql, $params);
        }

        public function queryParamsWithFetch($sql, $params){
            $result = mysql_query_params($this->con, $sql, $params);
            $rows = array();
            while ($r = mysql_fetch_assoc($result))
            {
                $rows[] = $r;
            }
            return $rows;
        }

        public function queryParamsWithFetchArray($sql, $params){
            $result = mysql_query_params($this->con, $sql, $params);
            $rows = array();
            while ($r = mysql_fetch_array($result)) {
                $rows[] = $r;
            }
            return $rows;
        }

    }
