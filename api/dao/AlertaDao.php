<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AlertaDao
 *
 * @author Mateus Pontes
 */
class AlertaDao {
    private $db;
    
    public function __contruct(){
        $this->db = new dataBaseHelper();
        $this->db->open();
    }
    
    public function getAll(){
        try {                         
            $sql = "select codigo, nome from TbListaCompra order by nome asc"; 
            $result = $this->db->query($sql);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function getMeusAlertas(){
        try {                         
            $sql = "select TbAlerta.codigoEmpresa, TbProduto.codigo codigoProduto, TbProduto.descricao, valor 
                    from TbAlerta
                    inner join TbProduto on TbProduto.codigo = TbAlerta.codigoProduto
                    where TbAlerta.codigoEmpresa = 1"; 
            
            $result = $this->db->query($sql);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function save($data){
        try {       
            
            if($data->codigo == 0){
               $sql = "insert into TbAlerta (codigoEmpresa, codigoProduto, valor)"
                . "             values (:codigoEmpresa, :codigoProduto, :valor)";
            
                $arrayParams = array(
                    'codigoEmpresa' => $data->codigoEmpresa,
                    'codigoProduto' => $data->codigoProduto,                    
                    'valor' => $data->valor
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
                $data->codigo = $result;
            }
            else{
                $sql = "update TbAlerta set valor = :valor where codigo = :codigo";
                
                $arrayParams = array(
                    'codigo' => $data->codigo,
                    'valor' => $data->valor
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
            }
            
            return $data->codigo;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }	
    
    public function delete($codigoEmpresa){
        $sql = "delete from TbAlerta where codigoEmpresa = :codigoEmpresa";
            
        $arrayParams = array(
           'codigoEmpresa' => $codigoEmpresa
        ); 
                
        $result = $this->db->queryParams($sql,$arrayParams);        
    }
    
    public function __dispose(){        
        $this->db->close();
    }
}
