<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ListaCompraDao
 *
 * @author Mateus Pontes
 */
class ListaCompraDao {
    private $db;
    
    public function __contruct(){
        $this->db = new dataBaseHelper();
        $this->db->open();
    }
    
    public function getAll(){
        try {                         
            $sql = "select codigo, nome from TbListaCompra order by nome asc"; 
            $result = $this->db->query($sql);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function getById($codigo){
        $sql = "select codigo, nome from TbListaCompra where codigo = :codigo order by nome asc";         
        $arrayParams = array(
            'codigo' => $codigo
        );
                
        $lista = $this->db->queryParams($sql,$arrayParams);
        
        $sql = "SELECT TbProduto.codigo codigoProduto, TbProduto.descricao produto, 
                TbComposicao.descricao composicao, TbListaCompraItem.quantidade
                FROM TbListaCompraItem
                inner join TbProduto on TbProduto.codigo = TbListaCompraItem.codigoProduto
                inner join TbComposicao on TbComposicao.codigo = TbProduto.codigoComposicao
                where TbListaCompraItem.codigoLista = :codigo";         
        $arrayParams = array(
            'codigo' => $codigo
        );
                
        $itens = $this->db->queryParams($sql,$arrayParams);
        
        return array (
                "lista" => $lista[0],
                "itens" => $itens
            );
    }
    
    public function getLists(){
        try {                         
            $sql = "select TbListaCompra.codigo, TbListaCompra.nome, count(TbListaCompra.nome) as qtde
                    from TbListaCompra
                    inner join TbListaCompraItem on TbListaCompraItem.codigoLista = TbListaCompra.codigo
                    where codigoEmpresa = 1 group by TbListaCompra.codigo, TbListaCompra.nome"; 
            $result = $this->db->query($sql);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function createList($data){
        if($data->codigo == 0){
               $sql = "insert into TbListaCompra (codigoUsuario, codigoEmpresa, nome, dataCadastro)"
                . "             values (:codigoUsuario, :codigoEmpresa, :nome, now())";
            
                $arrayParams = array(
                    'codigoUsuario' => 1,
                    'codigoEmpresa' => 1,                    
                    'nome' => $data->nome
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
                $data->codigo = $result;
            }
            else{
                $sql = "update TbListaCompra set nome = :nome where codigo = :codigo";
                
                $arrayParams = array(
                    'codigo' => $data->codigo,
                    'nome' => $data->nome
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
            }
            
            return $data->codigo;
    }
    
    public function addItemList($data, $codigoLista){
        $sql = "insert into TbListaCompraItem (codigoLista, codigoProduto, quantidade)"
                . "             values (:codigoLista, :codigoProduto, :quantidade)";
            
        $arrayParams = array(
            'codigoLista' => $codigoLista,
            'codigoProduto' => $data->codigoProduto,                    
            'quantidade' => $data->qtd
        ); 
                
        $result = $this->db->queryParams($sql,$arrayParams);
    }
    
    public function deleteList($codigoLista){
        $sql = "delete from TbListaCompraItem where codigoLista = :codigoLista";
            
        $arrayParams = array(
           'codigoLista' => $codigoLista
        ); 
                
        $result = $this->db->queryParams($sql,$arrayParams);        
    }
    
    
    
    
    
    public function __dispose(){        
        $this->db->close();
    }
}
