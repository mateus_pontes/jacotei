<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MarcaDao
 *
 * @author Mateus Pontes
 */
class MarcaDao {
     private $db;
    
    public function __contruct(){
        $this->db = new dataBaseHelper();
        $this->db->open();
    }
    
    public function getAll(){
        try {                         
            $sql = "SELECT * from TbMarca order by nome asc"; 
            $result = $this->db->query($sql);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function save($data){
        try {       
            
            if($data->codigo == 0){
               $sql = "insert into TbMarca (codigoUsuario, nome, imagem, dataCadastro)"
                . "             values (:codigoUsuario, :nome, :imagem, now())";
            
                $arrayParams = array(
                    'codigoUsuario' => $data->codigoUsuario,
                    'nome' => $data->nome,
                    'imagem' => $data->imagem
                ); 
            }
            else{
                $sql = "update TbMarca set nome = :nome, imagem = :imagem where codigo = :codigo";
                
                $arrayParams = array(                  
                    'nome' => $data->nome,
                    'imagem' => $data->imagem,
                    'codigo' => $data->codigo,
                );
            }
            
                
        
            $s = $this->db->queryParams($sql,$arrayParams);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function __dispose(){        
        $this->db->close();
    }
}
