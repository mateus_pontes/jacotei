<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ComposicaoDao
 *
 * @author mateus
 */
class ComposicaoDao {
    private $db;
    
    public function __contruct(){
        $this->db = new dataBaseHelper();
        $this->db->open();
    }
    
    public function getAll(){
        try {                         
            $sql = "SELECT * from TbComposicao order by codigo asc"; 
            $result = $this->db->query($sql);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function save($data){
        try {       
            
            if($data->codigo == 0){
               $sql = "insert into TbComposicao (descricao, sigla)"
                . "             values (:descricao, :sigla)";
            
                $arrayParams = array(
                    'descricao' => $data->descricao,
                    'sigla' => $data->sigla
                ); 
            }
            else{
                $sql = "update TbComposicao set descricao = :descricao, sigla = :sigla where codigo = :codigo";
                
                $arrayParams = array(                  
                    'descricao' => $data->descricao,
                    'sigla' => $data->sigla,
                    'codigo' => $data->codigo,
                );
            }
            
                
        
            $s = $this->db->queryParams($sql,$arrayParams);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function __dispose(){        
        $this->db->close();
    }
}

/*
public function getAll(){
        try {                         
            $sql = "SELECT * from TbComposicao where codigo = :codigo and descricao = :descricao";  
            
            $arrayParams = array(
                'codigo' => 2,
                'descricao' => 'pacotex'
            );
            
            $result = $this->db->queryParams($sql, $arrayParams);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
 *  */
