<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CotacaoManager
 *
 * @author Mateus Pontes
 */
class CotacaoManagerDao {
    private $db;
    
    public function __contruct(){
        $this->db = new dataBaseHelper();
        $this->db->open();
    }
    
    public function addCotacao($data){
        try {       
            
            $sql = "insert into TbCotacaoPesquisa (codigoEmpresa, codigoMercado, codigoCotador, dataCotacao)"
                . "             values (:codigoEmpresa, :codigoMercado, :codigoCotador, now())";
            
                $arrayParams = array(
                    'codigoEmpresa' => 1,
                    'codigoMercado' => $data->codigoMercado,                    
                    'codigoCotador' => 1
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function addItemCotacao($data){
        try {       
            
            if($data->codigo == 0){
               $sql = "insert into TbCotacaoPesquisaItem (codigoCotacao, codigoProduto, valorVarejo, valorAtacado, regra)"
                . "             values (:codigoCotacao, :codigoProduto, :valorVarejo, :valorAtacado, :regra)";
            
                $arrayParams = array(
                    'codigoCotacao' => $data->codigoCotacao,
                    'codigoProduto' => $data->codigoProduto,
                    'valorVarejo' => $data->valorVarejo,
                    'valorAtacado' => $data->valorAtacado,
                    'regra' => $data->regra,
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
                $data->codigo = $result;
            }
            else{
                $sql = "update TbCotacaoPesquisaItem set valorVarejo = :valorVarejo, valorAtacado = :valorAtacado, regra = :regra  where codigo = :codigo";
                
                $arrayParams = array(
                    'codigo' => $data->codigo,
                    'valorVarejo' => $data->valorVarejo,
                    'valorAtacado' => $data->valorAtacado,
                    'regra' => $data->regra,
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
            }
            
            return $data->codigo;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function __dispose(){        
        $this->db->close();
    }
}
