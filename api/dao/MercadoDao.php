<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MarcaDao
 *
 * @author Mateus Pontes
 */
class MercadoDao {
     private $db;
    
    public function __contruct(){
        $this->db = new dataBaseHelper();
        $this->db->open();
    }
    
    public function getAll(){
        try {                         
            $sql = "SELECT * from TbMercado order by nome asc"; 
            $result = $this->db->query($sql);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function save($data){
        try {       
            
            if($data->codigo == 0){
               $sql = "insert into TbMercado (nome, endereco, bairro, cidade, lat, lng, logo, dataCadastro)"
                . "             values (:nome, :endereco, :bairro, :cidade, :lat, :lng, :logo,  now())";
            
                $arrayParams = array(
                    'nome' => $data->nome,
                    'endereco' => $data->endereco,
                    'bairro' => $data->bairro,
                    'cidade' => $data->cidade,
                    'lat' => $data->location["lat"],
                    'lng' => $data->location["lng"],
                    'logo' => $data->logo,
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
                $data->codigo = $result;
            }
            else{
                $sql = "update TbMercado set nome = :nome, endereco = :endereco, bairro = :bairro, cidade = :cidade, lat = :lat, lng = :lng, logo = :logo where codigo = :codigo";
                
                $arrayParams = array(                                      
                    'codigo' => $data->codigo,
                    'nome' => $data->nome,
                    'endereco' => $data->endereco,
                    'bairro' => $data->bairro,
                    'cidade' => $data->cidade,
                    'lat' => $data->location["lat"],
                    'lng' => $data->location["lng"],
                    'logo' => $data->logo,
                );
                
                $result = $this->db->queryParams($sql,$arrayParams);
            }
            
            return $data->codigo;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function getId($codigo){
        try {
            
            $sql = "SELECT * from TbMercado where codigo = :codigo ";     
            
            $arrayParams = array(                  
                    'codigo' => $codigo
                );
            
            $result = $this->db->queryParams($sql,$arrayParams);
            
            return $result;
        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function __dispose(){        
        $this->db->close();
    }
}
