<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProdutoDao
 *
 * @author Mateus Pontes
 */
class ProdutoDao {
    private $db;
    
    public function __contruct(){
        $this->db = new dataBaseHelper();
        $this->db->open();
    }
    
    public function getAll(){
        try {                         
            $sql = "SELECT * from TbProduto order by nome asc"; 
            $result = $this->db->query($sql);
            
            return $result;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function getId($codigo){
        try {
            
            $sql = "SELECT * from TbProduto where codigo = :codigo ";     
            
            $arrayParams = array(                  
                    'codigo' => $codigo
                );
            
            $result = $this->db->queryParams($sql,$arrayParams);
            
            return $result;
        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function buscar($data){
        try {
            
            $where = " where 1 = 1";
            
            if($data->descricao != "")
                    $where = $where . " and TbProduto.descricao like '%" . $data->descricao . "%'";
            
            if($data->codigoMarca != "")
                    $where = $where . " and codigoMarca = " . $data->codigoMarca;
                       
            
            $sql = "SELECT TbProduto.*, TbMarca.nome, TbComposicao.sigla from TbProduto inner join TbMarca on TbMarca.codigo = TbProduto.codigoMarca inner join TbComposicao on TbComposicao.codigo = TbProduto.codigoComposicao " . $where . " order by TbProduto.descricao, TbMarca.nome asc";  
            
            
            $result = $this->db->query($sql);
            
            return $result;
        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function getFilterProd($filtro){
        try {
            
            $sql = "SELECT TbProduto.codigo codigo, TbProduto.descricao produto, TbComposicao.descricao composicao, TbComposicao.sigla, CASE TbMarca.nome WHEN  'clab' then '(mais barato)' ELSE TbMarca.nome END as marca,
                    concat(TbProduto.descricao, ' ' , CASE TbMarca.nome WHEN  'clab' then '(mais barato)' ELSE TbMarca.nome END, ' ', valorComposicao, ' ', TbComposicao.sigla) formatado
                    from TbProduto 
                    inner join TbMarca on TbMarca.codigo = TbProduto.codigoMarca 
                    inner join TbComposicao on TbComposicao.codigo = TbProduto.codigoComposicao where TbProduto.descricao like '%" . $filtro . "%' order by TbProduto.descricao, TbMarca.nome asc
                    ";  
            
            
            $result = $this->db->query($sql);
            
            return $result;
        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }
    
    public function save($data){
        try {       
            
            if($data->codigo == 0){
               $sql = "insert into TbProduto (codigoMarca, codigoComposicao, codigoUsuario, descricao, "
                       .                                "valorComposicao, ean, observacao, dataCadastro)"
                . "             values (:codigoMarca, :codigoComposicao, :codigoUsuario, :descricao, :valorComposicao, "
                                            . ":ean, :observacao, now())";
            
                $arrayParams = array(
                    'codigoMarca' => $data->codigoMarca,
                    'codigoComposicao' => $data->codigoComposicao,
                    'codigoUsuario' => 1,
                    'descricao' => $data->descricao,
                    'valorComposicao' => $data->valorComposicao,
                    'ean' => $data->ean,
                    'observacao' => $data->observacao,
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
                $data->codigo = $result;
            }
            else{
                $sql = "update TbProduto set codigoMarca = :codigoMarca, codigoComposicao = :codigoComposicao, descricao = :descricao, valorComposicao = :valorComposicao, ean = :ean, observacao = :observacao where codigo = :codigo";
                
                $arrayParams = array(
                    'codigo' => $data->codigo,
                    'codigoMarca' => $data->codigoMarca,
                    'codigoComposicao' => $data->codigoComposicao,                    
                    'descricao' => $data->descricao,
                    'valorComposicao' => $data->valorComposicao,
                    'ean' => $data->ean,
                    'observacao' => $data->observacao,
                ); 
                
                $result = $this->db->queryParams($sql,$arrayParams);
            }
            
                
        
            
            
            return $data->codigo;

        } catch (Exception $e) {
            error_log('['.date('Y-m-d H:i:s').'] - '.$e->getMessage().PHP_EOL, 3, "errorlog.log");
            $this->db->close();
            throw $e;
        }
    }	
    
    public function __dispose(){        
        $this->db->close();
    }
}
