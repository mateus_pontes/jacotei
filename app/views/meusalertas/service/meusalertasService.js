app.service('MeusAlertasApiService', function ($http) {
    var service = this;
        service.getAll = function(){
            return $http.get(__path + '/api/alerta').then(handleSuccess, handleError);
        }
        
        service.save = function(model){
            return $http.post(__path + '/api/alerta/add', model).then(handleSuccess, handleError);
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return { success: false, message: res.data.ExceptionMessage };
        }
    return service;
})