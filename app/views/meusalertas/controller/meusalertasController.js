app.controller('MeusAlertasController', function ($scope, MeusAlertasApiService, ngDialog, toastr) {


    $scope.aoEscolherProduto = function (produto) {
        $scope.produtoSelecionado = produto;
    };

    $scope.limparProduto = false;
    $scope.adicionarAlerta = function (produto) {
        $scope.alertas.push({
            produto: produto,
            valorAlerta: 0
        });
        $scope.limparProduto = true;
    };
    $scope.excluirAlerta = function (alerta) {
        $scope.alertas.splice($scope.alertas.indexOf(alerta), 1);
    };
    $scope.salvarListaAlertas = function (listaAlerta) {
        var model = {
            codigoEmpresa: 1,
            itens: [],
        };
        angular.forEach(listaAlerta, function (alerta, iA) {
            model.itens.push({
                codigo: 0,
                codigoEmpresa: 1,
                codigoProduto: alerta.produto.codigo,
                valor: alerta.valorAlerta
            });
        });
        MeusAlertasApiService.save(model).then(function (resultado) {
            toastr.success('Alerta','Registros salvos com sucesso!');
        }, function (error) {
            toastr.error('Alerta','Erro ao salva registros!');
            console.error(error);
        });
    };


    function init() {
        MeusAlertasApiService.getAll().then(function (resultado) {
            $scope.alertas = [];
            if (resultado.result.length > 0) {
                angular.forEach(resultado.result, function (alerta, iA) {
                    $scope.alertas.push({
                        codigo: alerta.codigo,
                        produto: {codigo: alerta.codigoProduto, formatado: alerta.descricao},
                        valorAlerta: alerta.valor
                    });
                });
            }
        }, function (error) {
            console.log(error);
        });
    }

    init();

});