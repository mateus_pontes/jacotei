app.controller('ComposicaoController', function ($scope, ComposicaoApiService, ngDialog, toastr) {
    $scope.composicao = {};
    
    ComposicaoApiService.getAll().then(function(response){
        $scope.composicoes = response;
    })
    
    $scope.add = function(item){
        if(item == 0)
            $scope.composicao.codigo = 0;
        else
            $scope.composicao = item;

        $scope.dialog = ngDialog.open({
            template: __path + '/app/views/composicao/modal-composicao.php',
            className: 'ngdialog-theme-default',        
            scope: $scope,
            closeByEscape: false
        });
    }
    
    $scope.salvar = function(formularioValido){
        if (!formularioValido){            
            toastr.error('Verifique os campos destacados', 'Error');
            return;
        } 
        
        ComposicaoApiService.save($scope.composicao).then(function(response){
            toastr.success('Atualização realizada com sucesso!', 'Sucesso');
            
            ComposicaoApiService.getAll().then(function(response){
                $scope.composicoes = response;
                $scope.dialog.close();
            })
        })
    }
    
    $scope.closeModal = function(){
        $scope.dialog.close();
    }
})