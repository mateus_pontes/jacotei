<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Composição dos Produtos</h4>
        <ol class="breadcrumb">
            <p class="text-muted m-b-20 font-13"><a href="#" class="waves-effect"><i class="icon-social-youtube fa-3x"></i></a> Tutorial</p>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
	<div class="card-box">
            
            <div class="pull-right">
                <a class="btn btn-sm btn-default" href="javascript:;" ng-click="add(0);"><i class="fa fa-plus"></i> Cadastrar Nova Composição</a>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <table wt-responsive-table class="table table-striped">
                        <thead>
                            <th>Código</th>
                            <th>Descrição</th>
                            <th>Sigla</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="item in composicoes">
                                <td><b>{{item.codigo}}</b></td>
                                <td>{{item.descricao}}</td>
                                <td>{{item.sigla}}</td>
                                <td align="right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%">
                                          Ações <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                          <li><a href="javascript:;" ng-click="add(item);"><i class="fa icon-pencil"></i>  Editar</a></li>                                          
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
	</div>
    </div>
</div>