app.service('ComposicaoApiService', function ($http) {
    var service = this;
        service.getAll = function(){
            return $http.get(__path + '/api/composicao').then(handleSuccess, handleError);
        }
        
        service.save = function(element){
            return $http.post(__path + '/api/composicao/add', element).then(handleSuccess, handleError);
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return { success: false, message: res.data.ExceptionMessage };
        }
    return service;
})