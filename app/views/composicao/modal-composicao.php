<h3>Manutenção Composição</h3>
<form class="form-group" name="form" novalidate ng-submit="salvar(form.$valid);">
    <div class="form-group">
	<label for="userName">Descrição</label>
        <input type="text" required placeholder="Informe a Descrição" name="descricao" ng-model="composicao.descricao" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.descricao.$invalid))}]" />
    </div>
    <div class="form-group" ng-class="[{'parsley-error': (form.$submitted && (form.sigla.$invalid))}]">
	<label for="userName">Sigla</label>
	<input type="text" required="" placeholder="Informe a Sigla" name="sigla" ng-model="composicao.sigla" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.sigla.$invalid))}]"/>
    </div>
<br>
    <div class="form-group text-right m-b-0">
	<button class="btn btn-primary waves-effect waves-light" type="submit">Salvar</button>
        <button ng-click="closeModal();" class="btn btn-danger waves-effect waves-light m-l-5">Cancelar</button>
    </div>					
</form>
