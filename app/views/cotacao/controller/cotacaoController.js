app.controller('CotacaoController', function ($scope, CotacaoService, ngDialog, toastr) {
    
    CotacaoService.getAllList().then(function(response){
        $scope.listasDeCompra = response.result;
    })
    
    $scope.listaDeCompra = {}
    
    $scope.showList = false
    
    $scope.listar = function(id) {
        CotacaoService.getList(id).then(function(response){
            $scope.listaDeCompra = response;
            $scope.showList = true;
        })
    }
})