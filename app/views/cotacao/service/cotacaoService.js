app.service('CotacaoService', function ($http) {
    var service = this;
        service.getAllList = function(){
            return $http.get(__path + '/api/listacompra').then(handleSuccess, handleError);
        }
        
        service.getList = function(id) {
            //return $http.get(__path + '/api/listacompra/'+id).then(handleSuccess, handleError);
            return $http.get(__path+'/app/views/cotacao/lista.json').then(handleSuccess,handleError);
        }
        
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return { success: false, message: res.data.ExceptionMessage };
        }
    return service;
})