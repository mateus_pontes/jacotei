<h2>Cotações</h2>
<form class="form row" ng-submit="listar(lista.codigo)">
    <div class="form-group col-xs-6">
        <select class="form-control" ng-model="lista" ng-options="l.nome for l in listasDeCompra">{{lista}}</select>
    </div>
    <div class="form-group col-xs-4">
        <button class="btn" type="submit">Comparar</button>
    </div>
</form>
<div ng-show="showList">
    <table class="table table-bordered">
        <thead>
            <tr >
                <th>Lista Itens</th>
                <th ng-repeat="mercado in listaDeCompra.itens[0].mercados">{{mercado.descricao}}</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="item in listaDeCompra.itens">
                <td>{{item.descricao}}</td>
                <td ng-class="{'success':mercado.menorValor}" ng-repeat="mercado in item.mercados">{{mercado.valor}}</td>
            </tr>
        </tbody>
    </table>
</div>
<div ng-show="showList">
    <table class="table table-bordered">
        <thead>
            <tr >
                <th>Item</th>
                <th>Mercado</th>
                <th>Valor</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="item in listaDeCompra.itens">
                <td>{{item.descricao}}</td>
                <td ng-if="mercado.menorValor" ng-repeat="mercado in item.mercados">{{mercado.descricao}}</td>
                <td ng-if="mercado.menorValor" ng-repeat="mercado in item.mercados">{{mercado.valor}}</td>
            </tr>
        </tbody>
    </table>
</div>