app.service('ListaCompraApiService', function ($http) {
    var service = this;
    service.getAll = function () {
        return $http.get(__path + '/api/mercado').then(handleSuccess, handleError);
    }

    service.getLista = function () {
        return $http.get(__path + '/api/listacompra/minhalista').then(handleSuccess, handleError);
    }

    service.getListaByCodigo = function (codigo) {
        return $http.post(__path + '/api/listacompra/codigo', {
            codigo: codigo
        }).then(handleSuccess, handleError);
    }

    service.getId = function (codigo) {
        return $http.get(__path + '/api/mercado/' + codigo).then(handleSuccess, handleError);
    }

    service.save = function (element) {
        return $http.post(__path + '/api/listacompra/add', element).then(handleSuccess, handleError);
    }

    function handleSuccess(res) {
        return res.data;
    }

    function handleError(res) {
        return {success: false, message: res.data.ExceptionMessage};
    }

    return service;
})