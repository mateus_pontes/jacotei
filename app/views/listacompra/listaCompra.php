<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Minhas Listas de Compra</h4>
        <ol class="breadcrumb">
            <p class="text-muted m-b-20 font-13"><a href="#" class="waves-effect"><i class="icon-social-youtube fa-3x"></i></a> Tutorial</p>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
	<div class="card-box">
            
            <div class="pull-right">
                <a href="#/manutencao-lista-compra/0" class="pull-right btn btn-default btn-sm waves-effect waves-light"><i class="fa fa-plus"></i> Criar Nova Lista</a></a>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <table wt-responsive-table class="table table-striped">
                        <thead>
                            <th>Nome</th>
                            <th>Quantidade Itens</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="lista in listas">
                                <td>{{lista.nome}}</td>
                                <td>{{lista.qtde}}</td>
                                <td align="right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%">
                                          Ações <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                          <li><a href="#/manutencao-lista-compra/{{lista.codigo}}"><i class="fa icon-pencil"></i>  Editar</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
	</div>
    </div>
</div>