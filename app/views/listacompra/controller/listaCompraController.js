app.controller('ListaCompraController', function ($scope, toastr, ListaCompraApiService, $q) {

    $scope.init = function () {
        $scope.listas = [];
        ListaCompraApiService.getLista().then(function (resultado) {
            if (resultado.result.length > 0) {
                $scope.listas = resultado.result;
            }
        }, function (erro) {

        });
    };

    $scope.init();

});

app.controller('ManutencaoListaCompraController', function ($scope, toastr, ListaCompraApiService, UploadService, $q, $routeParams) {

    $scope.aoEscolherProduto = function (produto) {
        $scope.produtoSelecionado = produto;
    };

    $scope.limparProduto = false;
    $scope.adicionarItemProduto = function (produto) {
        $scope.itens.push({
            produto: produto
        });
        $scope.limparProduto = true;
    };
    $scope.excluirItemProduto = function (item) {
        $scope.itens.splice($scope.itens.indexOf(item), 1);
    };
    $scope.salvarListaItensProduto = function (listaitem) {
        var model = {
            nome: listaitem.nome,
            codigo: listaitem.codigo,
            itens: [],
        };
        angular.forEach(listaitem, function (item, iI) {
            model.itens.push({
                codigoProduto: item.produto.codigo,
                qtd: item.quantidade
            });
        });
        ListaCompraApiService.save(model).then(function (resultado) {
            toastr.success('item', 'Registros salvos com sucesso!');
        }, function (error) {
            toastr.error('item', 'Erro ao salva registros!');
            console.error(error);
        });
    };

    $scope.init = function () {

        $scope.itens = [];
        ListaCompraApiService.getListaByCodigo($routeParams.codigo).then(function (resultado) {
            $scope.itens.nome = resultado.result.lista.nome;
            $scope.itens.codigo = resultado.result.lista.codigo;
            angular.forEach(resultado.result.itens, function (alerta, iA) {
                $scope.itens.push({
                    produto: {
                        formatado: alerta.produto,
                        codigo: alerta.codigoProduto,
                        composicao: alerta.composicao
                    },
                    quantidade: parseInt(alerta.quantidade)
                });
            });
            console.log(resultado);
        }, function (erro) {
            console.erro(erro);
        });


    };

    $scope.init();
})