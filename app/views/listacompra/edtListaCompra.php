<style>
    #map {
        height: 200px;
        border: solid 1px black;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Minha Lista de Campra</h4>
        <ol class="breadcrumb">
            <p class="text-muted m-b-20 font-13"><a href="#" class="waves-effect"><i class="icon-social-youtube fa-3x"></i></a> Tutorial</p>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <form class="form-horizontal" name="form" novalidate ng-submit="salvar(form.$valid);">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Nome da Lista</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="exemplo. (Lista do Fim de Semana)" required ng-model="itens.nome" name="nome" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.nome.$invalid))}]"/>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="row">
                    <div class="col-xs-12">
                        <hr>
                        <h4 class="text-dark header-title m-t-0 m-b-30">Itens da Minha Lista</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-xs-12 col-md-4 form-group">
                                    <busca-produto-diretiva ao-escolher-produto-callback="aoEscolherProduto" limpar-produto="limparProduto"></busca-produto-diretiva>
                                </div>
                                <div class="col-xs-12 col-md-4 form-group">
                                    <label class="control-label"></label>
                                    <button ng-disabled="!produtoSelecionado" class="btn btn-default waves-effect waves-light" ng-click="adicionarItemProduto(produtoSelecionado)" style="display: block;"><i class="fa fa-plus"></i> Adicionar</button>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table wt-responsive-table class="table table-striped">
                                        <thead>
                                        <th>Produto</th>
                                        <th>Composição</th>
                                        <th>Quantidade</th>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="item in itens">
                                            <td>{{item.produto.formatado}}</td>
                                            <td>{{item.produto.composicao}}</td>
                                            <td><input type="number" ng-model="item.quantidade"></td>
                                            <td align="right">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%">
                                                        Ações <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li><a ng-click="excluirItemProduto(item)"><i class="fa fa-remove"></i> Excluir </a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="pull-right">
                                        <a class="pull-right btn btn-default btn-sm waves-effect waves-light" ng-click="salvarListaItensProduto(itens)"><i class="fa fa-save"></i> Salvar </a></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="col-md-3">
        <div ng-show="logo">
            <img src="{{logo}}" width="100%"/>
        </div>
    </div>
</div>