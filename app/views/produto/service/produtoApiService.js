app.service('ProdutoApiService', function ($http) {
    var service = this;
        
        service.save = function(element){
            return $http.post(__path + '/api/produto/add', element).then(handleSuccess, handleError);
        }
        
        service.buscar = function(filter){
            return $http.post(__path + '/api/produto/buscar', filter).then(handleSuccess, handleError);
        }
        
        service.getId = function(codigo){
            return $http.get(__path + '/api/produto/' + codigo).then(handleSuccess, handleError);
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return { success: false, message: res.data.ExceptionMessage };
        }
    return service;
})