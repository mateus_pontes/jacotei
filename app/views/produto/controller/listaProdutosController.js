app.controller('ListaProdutosController', function ($scope, toastr, MarcaApiService, ProdutoApiService, $q) {
    $scope.lista = [];
    $scope.filter = {};
    $scope.filter.descricao = "";
    $scope.filter.codigoMarca = "";
    $scope.filter.codigo = "";
    
    $scope.buscar = function(){
        ProdutoApiService.buscar($scope.filter).then(function(response){
            $scope.produtos = response.result;
        })
    }
    
    $scope.init = function(){
        $scope.carregaCombos().then(function(response){
            
        })
    }
    
    $scope.carregaCombos = function(){
        var promises = [];
        
        promises.push(MarcaApiService.getAll().then(function(response){
            $scope.marcas = response;
        }));
        return $q.all(promises);
    }
    
    $scope.init();
})