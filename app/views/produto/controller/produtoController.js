app.controller('ProdutoController', function ($scope, toastr, $routeParams, 
                                MarcaApiService, ComposicaoApiService, $q, ProdutoApiService) {
    $scope.produto = {};
    $scope.produto.codigo = $routeParams.codigo;
    
    $scope.init = function(){
        $scope.carregaCombos().then(function(response, o){
            
        })
    }
    
    $scope.salvar = function(valido){
        if(!valido){
            toastr.error("Os campos em destaque são obrigatórios");
            return;
        }
        
        ProdutoApiService.save($scope.produto).then(function(response){
            if(response.error){
                toastr.error("Erro ao atualizar " + response.message, "Error");
                return;
            }
            
            $scope.produto.codigo = response.codigo;
            toastr.success("Registro atualizado com sucesso!", "Sucesso");
        })
    }
    
    $scope.carregaCombos = function(){
        var promises = [];
        
        promises.push(MarcaApiService.getAll().then(function(response){
            $scope.marcas = response;
        }));
        
        promises.push(ComposicaoApiService.getAll().then(function(response){
            $scope.composicao = response;
        }));
        
        if($scope.produto.codigo > 0){
            promises.push(ProdutoApiService.getId($scope.produto.codigo).then(function(response){
                $scope.produto = response.result[0];
            }));
        }
        
        return $q.all(promises);
    }
    
    $scope.init();
})