<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Cadastro de Produtos</h4>
        <ol class="breadcrumb">
            <p class="text-muted m-b-20 font-13"><a href="#" class="waves-effect"><i class="icon-social-youtube fa-3x"></i></a> Tutorial</p>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <a href="#/cadastro-produtos/0" class="pull-right btn btn-default btn-sm waves-effect waves-light"><i class="fa fa-plus"></i> Cadastrar Novo Produto</a></a>
            <h4 class="text-dark header-title m-t-0 m-b-30">Pesquisar</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="edtDescricao">Descrição</label>
                            <input type="text" id="edtDescricao" class="form-control" placeholder="Descrição do Produto" ng-model="filter.descricao">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cboMarcas">Marcas</label>
                            <select class="form-control" ng-model="filter.codigoMarca">
                                <option value="" selected>selecione...</option>
                                <option ng-repeat="item in marcas" value="{{item.codigo}}">{{item.nome}}</option>
                            </select>
                        </div>
                    </div>
                    <center>
                        <button type="button" class="btn btn-primary waves-effect waves-light" ng-click="buscar();">Pesquisar</button>
                    </center>
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
	<div class="card-box">
            <h4 class="text-dark header-title m-t-0 m-b-30">Resultado da Busca <span ng-show="produtos.length > 0" style="font-size: 12px;"><b>{{produtos.length}} registro(s)</b></span></h4>                        
            <div class="row">
                <div class="col-md-12">
                    <table wt-responsive-table class="table table-striped">
                        <thead>
                            <th>Descrição</th>
                            <th>Marca</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="item in produtos">
                                <td><b>{{item.descricao}} - {{item.valorComposicao}} {{item.sigla}}</b></td>
                                <td>{{item.nome}}</td>
                                <td align="right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%">
                                          Ações <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                          <li><a href="#/cadastro-produtos/{{item.codigo}}"><i class="fa icon-pencil"></i>  Editar</a></li>                                          
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
	</div>
    </div>
</div>