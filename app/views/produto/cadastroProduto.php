<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Cadastro de Produto</h4>
        <ol class="breadcrumb">
            <p class="text-muted m-b-20 font-13"><a href="#" class="waves-effect"><i class="icon-social-youtube fa-3x"></i></a> Tutorial</p>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0 m-b-30">Cadastro de Produto</h4>
            <div class="row">
                <form class="form-horizontal" name="form" novalidate ng-submit="salvar(form.$valid);">                                   
	            <div class="form-group">
	                <label class="col-md-2 control-label">Descrição</label>
	                <div class="col-md-10">
                            <input type="text" name="descricao" required placeholder="Descrição do Produto" ng-model="produto.descricao" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.descricao.$invalid))}]" />
	                </div>
	            </div>
                    <div class="form-group">
	                <label class="col-md-2 control-label">Código de Barra</label>
	                <div class="col-md-10">
                            <input type="text" class="form-control" placeholder="Código de Barra" required ng-model="produto.ean" name="ean" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.ean.$invalid))}]" />
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-md-2 control-label" for="example-email">Marca</label>
	                <div class="col-md-10">
                            <select class="form-control" ng-model="produto.codigoMarca" required name="marca" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.marca.$invalid))}]" />
                                <option value="">selecione...</option>
                                <option ng-repeat="item in marcas" value="{{item.codigo}}">
                                                {{item.nome}}
                                </option>
                            </select>
	                </div>
	            </div>  
                    <div class="form-group">
	                <label class="col-md-2 control-label" for="example-email">Composição</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" placeholder="Quantidade" style="text-align: right" ng-model="produto.valorComposicao" required name="valorcomposicao" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.valorcomposicao.$invalid))}]" />
	                </div>
	                <div class="col-md-5">
                            <select ng-model="produto.codigoComposicao" required name="codigoComposicao" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.codigoComposicao.$invalid))}]" />
                                <option value="">selecione...</option>
                                 <option ng-repeat="item in composicao" value="{{item.codigo}}">
                                                {{item.sigla}} ({{item.descricao}})
                                </option>
                            </select>
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-md-2 control-label">Observações</label>
	                <div class="col-md-10">
	                    <textarea class="form-control" rows="5" ng-model="produto.observacao"></textarea>
	                </div>
	            </div>
                    <a href="#/lista-produtos" class="pull-left btn btn-primary btn waves-effect waves-light"><i class="fa fa-backward"></i> Voltar</a></a>                    
                    <button type="submit" class="btn btn-success waves-effect waves-light pull-right">Salvar</button>                    
                </form>
            </div>
        </div>
    </div>
</div>