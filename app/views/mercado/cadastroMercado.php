<style>
      #map {
        height: 200px;
        border: solid 1px black;
      }
    </style>
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Cadastro de Mercado</h4>
        <ol class="breadcrumb">
            <p class="text-muted m-b-20 font-13"><a href="#" class="waves-effect"><i class="icon-social-youtube fa-3x"></i></a> Tutorial</p>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-9">
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0 m-b-30">Cadastro de Mercado</h4>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" name="form" novalidate ng-submit="salvar(form.$valid);">                                   
	            <div class="form-group">
	                <label class="col-md-2 control-label">Nome</label>
	                <div class="col-md-10">
                            <input type="text" class="form-control" placeholder="Nome" required ng-model="mercado.nome" name="nome" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.nome.$invalid))}]" />
	                </div>
	            </div>
                    <div class="form-group">
	                <label class="col-md-2 control-label">Endereço</label>
	                <div class="col-md-10">
                            <input type="text" class="form-control" placeholder="Endereço" required ng-model="mercado.endereco" name="endereco" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.endereco.$invalid))}]" />
	                </div>
	            </div>
                    <div class="form-group">
	                <label class="col-md-2 control-label">Bairro</label>
	                <div class="col-md-10">
                            <input type="text" class="form-control" placeholder="Bairro" required ng-model="mercado.bairro" name="bairro" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.bairro.$invalid))}]" />
	                </div>
	            </div>
                    <div class="form-group">
	                <label class="col-md-2 control-label">Cidade</label>
	                <div class="col-md-10">
                            <input type="text" class="form-control" placeholder="Cidade" required ng-model="mercado.cidade" name="cidade" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.cidade.$invalid))}]" />
	                </div>
	            </div>                    
                    <div class="form-group">
	                <label class="col-md-2 control-label">Logo</label>
	                <div class="col-md-10">
                            <input type="file" ng-model="mercado.base64" name="ucShape" style="height: 32px;" class="form-control" accept=".png,.jpg,.gif" maxsize="2000" base-sixty-four-input parser="onLoadComplete">
	            </div>
	            </div>  
                    <a href="#/lista-mercados" class="pull-left btn btn-primary btn waves-effect waves-light"><i class="fa fa-backward"></i> Voltar</a></a>                    
                    <button type="submit" class="btn btn-success waves-effect waves-light pull-right">Salvar</button>                    
                </form>
                </div>                
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div ng-show="logo">
            <img src="{{logo}}" width="100%" />
        </div>        
    </div>
</div>