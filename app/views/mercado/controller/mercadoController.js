app.controller('ListaMercadosController', function ($scope, toastr, MercadoApiService, $q) {
    MercadoApiService.getAll().then(function(response){
        $scope.mercados = response;
    })
});

app.controller('MercadoController', function ($scope, toastr, MercadoApiService, UploadService, $q, $routeParams) {
    $scope.mercado = {};
    $scope.mercado.codigo = $routeParams.codigo;
    $scope.mercado.logo = "";

    $scope.logo = "";

    $scope.onLoadComplete = function(file, base64_object ){
        UploadService.image(base64_object).then(function(response){
            $scope.mercado.logo = response.path;
            $scope.logo = __path + "/api/" + response.path;

            jQuery("input[type='file']").val(null);
        })
    };

    $scope.salvar = function(valido){
        if(!valido){
            toastr.error("Os campos em destaque são obrigatórios");
            return;
        }

        MercadoApiService.save($scope.mercado).then(function(response){
            if(response.error){
                toastr.error("Erro ao atualizar " + response.message, "Error");
                return;
            }

            $scope.mercado.codigo = response.codigo;
            toastr.success("Registro atualizado com sucesso!", "Sucesso");
        })
    };

    $scope.init = function(){
        if($scope.mercado.codigo > 0){
            MercadoApiService.getId($scope.mercado.codigo).then(function(response){
                $scope.mercado = response.result[0];
                $scope.logo = __path + "/api/" + $scope.mercado.logo;
            })
        }
    };

    $scope.init();
})