<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Cotação Manager</h4>
        <ol class="breadcrumb">
            <p class="text-muted m-b-20 font-13"><a href="#" class="waves-effect"><i class="icon-social-youtube fa-3x"></i></a> Tutorial</p>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
	    <div class="card-box">
            <div class="col-md-6" ng-init="getMercado()" id="divMercado">
                <h4>Mercado:
                    <div class="input-group" id="selectMercado" ng-hide="mercados.mercadox">
                    <ui-select ui-disable-choice ng-change="iniciarCotacao()" ng-model="mercados.mercadox" class="{{loading?'options-loading':''}}" on-select="aoSelecionarProduto($select.selected)" theme="bootstrap" ng-disabled="disabled" reset-search-input="false">
                                <ui-select-match placeholder="Selecione...">{{$select.selected.nome}}</ui-select-match>
                                <ui-select-choices repeat="item in mercados | filter: $select.search"
                                                   refresh="aoDigitarDescricao($select.search)"
                                                   refresh-delay="0">
                                    <span ng-bind-html="item.nome | highlight: $select.search">{{item.nome}}</span>
                                </ui-select-choices>
                            </ui-select>
                            <span ng-click="iniciarCotacao()" style="cursor: pointer" class="input-group-addon btn-default" id="basic-addon2">INICIAR</span>
                    </div>
                    <div ng-show="mercados.mercadox">
                        <h2>{{mercados.mercadox.nome}}</h2>
                    </div>
                </h4>
            </div>
            <div class="clearfix"></div>
            <br/>
            <div class="col-xs-9 col-md-4 form-group" ng-init="getMercado()" id="divProduto" ng-show="mercados.mercadox">
                <h4>Produto:
                    <busca-produto-diretiva  ao-escolher-produto-callback="aoEscolherProduto"></busca-produto-diretiva>
                </h4>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <th>Produto</th>
                            <th>Varejo</th>
                            <th>Atacado</th>
                            <th>Regra</th>
                            <th></th>
                        </thead>
                        <tbody ng-repeat="item in produtosCotacao">
                            <tr>
                                <td><b>{{item.nomeProduto}}</b></td>
                                <td>{{item.valorVarejo}}</td>
                                <td>{{item.valorAtacado}}</td>
                                <td>{{item.regra}}</td>
                                <td align="right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%">
                                          Ações <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                          <li><a href="javascript:;" ng-click="editar(item)"><i class="fa icon-pencil"></i>  Editar</a></li>
                                          <li><a href="javascript:;" ng-click="excluir(item)"><i class="fa icon-pencil"></i>  Excluir</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
	    </div>
    </div>
</div>