app.controller('CotacaoManagerController', function ($scope, CotacaoManagerApiService, ngDialog, toastr) {

    $scope.cotacao = {
        codigo: 0,
        codigoCotacao: '0',
        codigoProduto: '',
        valorVarejo: '',
        valorAtacado: '',
        regra: ''
    };
    $scope.produtosCotacao = [];

    CotacaoManagerApiService.getMercado().then(function (response) {
        $scope.mercados = response;
    });

    $scope.iniciarCotacao = function () {
        var mercado = {
            codigoMercado:$scope.mercados.mercadox.codigo
        };
        CotacaoManagerApiService.iniciarCotacao(mercado).then(function (response) {
            $scope.cotacao.codigoCotacao = response.codigoCotacao;
        });
    };

    $scope.aoEscolherProduto = function (produto) {
        $scope.produto = produto;
        $scope.sigla = produto.sigla;
        $scope.nomeProd = produto.formatado;
        $scope.composicao = produto.composicao;
        $scope.cotacao.valorVarejo = '';
        $scope.cotacao.valorAtacado = '';
        $scope.cotacao.regra = '';
        $scope.cotacao.codigo = 0;
        $scope.dialog = ngDialog.open({
            template: __path + '/app/views/cotacaomanager/modal-cotacaomanager.php',
            className: 'ngdialog-theme-default',
            scope: $scope,
            closeByEscape: false
        });
    };

    $scope.salvar = function () {
        $scope.cotacao.codigoProduto = $scope.produto.codigo;
        console.log($scope.cotacao);
        $scope.dialog.close();
        CotacaoManagerApiService.salvarCotacao($scope.cotacao).then(function (response) {
            console.log(response)
            var retorno = $scope.cotacao;
            retorno.codigo = response.codigo;
            retorno['nomeProduto'] = $scope.produto.formatado;
            retorno['sigla'] = $scope.produto.sigla;
            retorno['composicao'] = $scope.produto.composicao;
            var lista = [];
            $scope.produtosCotacao.forEach(function(item){
                if (item.codigo!=retorno.codigo){
                    lista.push(item);
                }
            })
            lista.push(angular.copy(retorno));
            $scope.produtosCotacao = lista;
        });

    };

    $scope.editar = function (item) {
        $scope.sigla = item.sigla;
        $scope.nomeProd = item.nomeProduto;
        $scope.produto.formatado = item.nomeProduto;
        $scope.composicao = item.composicao;
        $scope.cotacao.valorVarejo = item.valorVarejo;
        $scope.cotacao.valorAtacado = item.valorAtacado;
        $scope.cotacao.regra = item.regra;
        $scope.cotacao.codigo = item.codigo;
        $scope.dialog = ngDialog.open({
            template: __path + '/app/views/cotacaomanager/modal-cotacaomanager.php',
            className: 'ngdialog-theme-default',
            scope: $scope,
            closeByEscape: false
        });
        console.log('editar: ' + item.nomeProduto);
    };

    $scope.excluir = function (item) {
        console.log('excluir: ' + item.nomeProduto);
    };

    $scope.closeModal = function () {
        $scope.dialog.close();
    };
});