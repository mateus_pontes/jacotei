<h2>Cotação Manager</h2>
<h4>{{nomeProd}} - {{sigla}}</h4>
<form class="form-group" name="form" novalidate ng-submit="salvar(form.$valid);">
    <fieldset class="form-group ">
        <label for="exampleInputEmail1">Registre os valores:</label>
        <div class="input-group col-md-6">
          <span class="input-group-addon" id="basic-addon3">Varejo: R$</span>
          <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" ng-model="cotacao.valorVarejo">
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="input-group col-md-6">
              <span class="input-group-addon" id="basic-addon3">Atacado: R$</span>
              <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" ng-model="cotacao.valorAtacado">
        </div>
    </fieldset>
    <br/>
    <fieldset class="form-group ">
        <label for="exampleInputEmail1">Regra</label>
        <div class="input-group col-md-6">
              <span class="input-group-addon" id="basic-addon3">{{composicao}} ></span>
              <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" ng-model="cotacao.regra">
        </div>
      </fieldset>
<br>
    <div class="form-group text-right m-b-0">
	<button class="btn btn-primary waves-effect waves-light" type="submit">Salvar</button>
        <button ng-click="closeModal();" class="btn btn-danger waves-effect waves-light m-l-5">Cancelar</button>
    </div>					
</form>
