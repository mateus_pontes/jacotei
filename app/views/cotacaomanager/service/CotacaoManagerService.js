app.service('CotacaoManagerApiService', function ($http) {
    var service = this;
    //Mudar nome função
    service.getMercado = function(){
        return $http.get(__path + '/api/mercado').then(handleSuccess, handleError);
    };

    service.getProdutos = function(filter){
        //retornar todos produtos
        return $http.post(__path + '/api/produto/filtro', filter).then(handleSuccess, handleError);
    };

    service.iniciarCotacao = function(objeto){
        return $http.post(__path + '/api/cotacao/add', objeto).then(handleSuccess, handleError);
    };

    service.salvarCotacao = function(objeto){
        return $http.post(__path + '/api/cotacao/item/add', objeto).then(handleSuccess, handleError);
    }

    function handleSuccess(res) {
        return res.data;
    }

    function handleError(res) {
        return { success: false, message: res.data.ExceptionMessage };
    }
    return service;
})