<h3>Manutenção Composição</h3>
<form class="form-group" name="form" novalidate ng-submit="salvar(form.$valid);">
    <div class="form-group">
	<label for="userName">Nome</label>
        <input type="text" required placeholder="Informe o Nome" name="nome" ng-model="marca.nome" ng-class="['form-control',{'parsley-error': (form.$submitted && (form.nome.$invalid))}]" />
    </div>
<br>
    <div class="form-group text-right m-b-0">
	<button class="btn btn-primary waves-effect waves-light" type="submit">Salvar</button>
        <button ng-click="closeModal();" class="btn btn-danger waves-effect waves-light m-l-5">Cancelar</button>
    </div>					
</form>
