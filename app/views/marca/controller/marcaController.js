app.controller('MarcaController', function ($scope, MarcaApiService, ngDialog, toastr) {
    $scope.marca = {};
    
    MarcaApiService.getAll().then(function(response){
        $scope.marcas = response;
    })
    
    $scope.add = function(item){
        if(item == 0)
            $scope.marca.codigo = 0;
        else
            $scope.marca = item;
        
        $scope.marca.codigoUsuario = 1;
        $scope.marca.imagem = "";

        $scope.dialog = ngDialog.open({
            template: __path + '/app/views/marca/modal-marca.php',
            className: 'ngdialog-theme-default',        
            scope: $scope,
            closeByEscape: false
        });
    }
    
    $scope.salvar = function(formularioValido){
        if (!formularioValido){            
            toastr.error('Verifique os campos destacados', 'Error');
            return;
        } 
        
        MarcaApiService.save($scope.marca).then(function(response){
            toastr.success('Atualização realizada com sucesso!', 'Sucesso');
            
            MarcaApiService.getAll().then(function(response){
                $scope.marca = {};
                $scope.marcas = response;
                $scope.dialog.close();
            })
        })
    }
    
    $scope.closeModal = function(){
        $scope.dialog.close();
    }
})