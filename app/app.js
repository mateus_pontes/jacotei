var app = angular.module('app', [
    'ngRoute',
    'ngDialog',
    'toastr',
    'naif.base64',
    'angucomplete',
    'wt.responsive',
    'ui.select',    
    'ui.utils.masks'
]);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when(
            '/',
            {
                templateUrl: "app/views/dashboard/dashboard.php",
                controller: "DashBoardController"
            }
        )
        .when(
            '/composicao',
            {
                templateUrl: "app/views/composicao/composicao.php",
                controller: "ComposicaoController"
            }
        )
        .when(
            '/marca',
            {
                templateUrl: "app/views/marca/listaMarca.php",
                controller: "MarcaController"
            }
        )
        .when(
            '/lista-produtos',
            {
                templateUrl: "app/views/produto/listaProduto.php",
                controller: "ListaProdutosController"
            }
        )
        .when(
            '/cadastro-produtos/:codigo',
            {
                templateUrl: "app/views/produto/cadastroProduto.php",
                controller: "ProdutoController"
            }
        )
        .when(
            '/lista-mercados',
            {
                templateUrl: "app/views/mercado/listaMercado.php",
                controller: "ListaMercadosController"
            }
        )
        .when(
            '/cadastro-mercado/:codigo',
            {
                templateUrl: "app/views/mercado/cadastroMercado.php",
                controller: "MercadoController"
            }
        )
        .when(
            '/lista-compra',
            {
                templateUrl: "app/views/listacompra/listaCompra.php",
                controller: "ListaCompraController"
            }
        )
        .when(
            '/manutencao-lista-compra/:codigo',
            {
                templateUrl: "app/views/listacompra/edtListaCompra.php",
                controller: "ManutencaoListaCompraController"
            }
        )
        .when('/cotacao-manager',
            {
                templateUrl: "app/views/cotacaomanager/cadastroCotacaoManager.php",
                controller: "CotacaoManagerController"
            }
        )
        .when('/meus-alertas',
            {
                templateUrl: "app/views/meusalertas/meusalertas.html",
                controller: "MeusAlertasController"
            }
        )
        .when('/cotacao',
            {
                templateUrl: "app/views/cotacao/cotacao.php",
                controller: "CotacaoController"
            }   
        )
        // caso não seja nenhum desses, redirecione para a rota '/'
        .otherwise({redirectTo: '/'});
});