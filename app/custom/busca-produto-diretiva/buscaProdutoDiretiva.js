angular.module('angucomplete', [])
    .directive('buscaProdutoDiretiva', function (buscaProdutoApiService) {

        return {
            restrict: 'E',
            replace: true,
            scope: {
                aoEscolherProdutoCallback: '=',
                limparProduto: '='
            },
            controllerAs: 'controller',
            templateUrl: 'app/custom/busca-produto-diretiva/buscaProdutoDiretiva.html',
            link: function (scope, element, attrs) {


                scope.produtos = [];
                scope.selecionado = {};


                scope.aoSelecionarProduto = function (produto) {
                    scope.aoEscolherProdutoCallback(produto);
                };

                scope.loading = false;

                scope.aoDigitarDescricao = function (descricao) {
                    if (descricao.length == 0) {
                        scope.produtos = [];
                    }
                    if (descricao.length >= 3) {
                        scope.loading = true;
                        buscaProdutoApiService.getByDescription(descricao).then(function (resultado) {
                            scope.loading = false;
                            scope.produtos = resultado.result;
                        }, function (erro) {
                            console.error(erro);
                        });
                    }
                };

                scope.$watch(function () {
                    return scope.limparProduto;
                }, function () {
                    if (scope.limparProduto) {
                        scope.selecionado = {};
                        scope.limparProduto = false;
                    }
                });


            }
        }

    });