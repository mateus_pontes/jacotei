app.service('buscaProdutoApiService', function ($http) {
    var service = this;

    service.getByDescription = function (description) {
        return $http.post(__path + '/api/produto/filtro', {
            filtro: description
        }).then(handleSuccess, handleError);
    };

    function handleSuccess(res) {
        return res.data;
    };

    function handleError(res) {
        return {success: false, message: res.data.ExceptionMessage};
    };

    return service;
});