app.service('UploadService', function ($http) {
    var service = this;
       service.image = function(element){
            return $http.post(__path + '/api/upload/image', element).then(handleSuccess, handleError);
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return { success: false, message: res.data.ExceptionMessage };
        }
    return service;
})